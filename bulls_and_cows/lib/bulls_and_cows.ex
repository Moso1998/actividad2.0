defmodule BullsAndCows do
  def contadorbulls([hs|ts], [hg|tg]) do
    cond do
      ts == [] and hs == hg -> 1
      ts == [] and hs != hg -> 0
      hs == hg -> BullsAndCows.contadorbulls(ts, tg) + 1
      hs != hg -> BullsAndCows.contadorbulls(ts, tg)
    end

    def contadorcows([hs|ts], [hg|tg]) do
      cond do
        ts == [] and hs == hg -> 1
        ts == [] and hs != hg -> 0
        hs == hg -> BullsAndCows.contadorcows(ts, tg) + 1
        hs != hg -> BullsAndCows.contadorcows(ts, tg)
      end

  end
  def score_guess(secret, guess) do
    secretl = String.codepoints(secret)
    guessl = String.codepoints(guess)
    bulls = BullsAndCows.contadorbulls(secretl, guessl)
    "#{bulls} Bulls, 0 Cows"
  end
end
